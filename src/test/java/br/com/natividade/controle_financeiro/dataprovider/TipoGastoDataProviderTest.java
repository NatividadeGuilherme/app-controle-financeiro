package br.com.natividade.controle_financeiro.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.TipoGastoRepository;
import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

@DataJpaTest
public class TipoGastoDataProviderTest {

	@Autowired
	private TipoGastoRepository repository;
	
	private TipoGastoDataProvider dataProvider;
	
	@BeforeEach
	public void setUp() {
		dataProvider = new TipoGastoDataProvider(repository);
	}
	
	@Test
	public void dadoQueTenhoTipoGastoPreenchido_entaoEfetuaGravacao() {
		// cenário
		TipoGastoDomain domain = mockTipoGastoDomain();
		
		// ação
		dataProvider.adicionaTipoGasto(domain);
		TipoGastoModel tipoModel = repository.findAll().get(0);
		
		// validação
		assertNotNull(tipoModel);
		assertEquals("TRANSPORTE", tipoModel.getDescricao());
		
	}	
	
	@Test
	public void dadoQueTenhoTipoGastoDadosInvalidos_entaoLancaException() {
		// Cenário
		TipoGastoDomain tipoDomain = mockTipoGastoDomain();
		tipoDomain.setDescricao("");
		
		// ação e validação
		RuntimeException error = assertThrows(RuntimeException.class, () ->{
			dataProvider.adicionaTipoGasto(tipoDomain);
		});
		
		assertEquals("Ocorreu um erro ao realizar a gravação do tipo de gasto", error.getMessage());
	}
	
	@Test
	public void dadoQueTenhoTiposGastosCadastrados_entaoDevemSerRetornados() {
		// cenário
		repository.saveAndFlush(new TipoGastoModel("UBER"));
		
		// ação
		List<TipoGastoDomain> tipos = dataProvider.getTipos();
		
		// validação
		assertEquals(1, tipos.size());
		assertEquals("UBER", tipos.get(0).getDescricao());
	}
	
	private TipoGastoDomain mockTipoGastoDomain() {
		return new TipoGastoDomain("Transporte");
	}
}
