package br.com.natividade.controle_financeiro.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoModel;
import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.CaixaRepository;
import br.com.natividade.controle_financeiro.dataprovider.repository.MovimentoRepository;
import br.com.natividade.controle_financeiro.dataprovider.repository.TipoGastoRepository;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.parameters.ParametrosConsultaMovimentoDomain;

@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class MovimentoDataProviderTest {

	@Autowired
	private MovimentoRepository repository;

	@Autowired
	private EntityManager em;

	@Autowired
	private TipoGastoRepository tipoGastoRepository;

	private CaixaDataProvider caixaDataProvider;

	@Autowired
	private CaixaRepository caixaRepository;

	private MovimentoDataProvider dataprovider;

	private CaixaModel caixaModel;

	private UUID uuidCaixa = UUID.fromString("0351dc8c-4015-44a8-b491-6f3ad6e71b5d");

	@BeforeEach
	public void setUp() {
		caixaDataProvider = new CaixaDataProvider(caixaRepository);
		dataprovider = new MovimentoDataProvider(repository, caixaDataProvider, em, null, null);
		caixaModel = new CaixaModel("EMERGENCIAL", new BigDecimal("15.00"), uuidCaixa);
	}

	@Test
	public void dadoQueTenhoMovimentoPreenchido_entaoGravaOhMovimento() {
		// cenário
		MovimentoDomain movimentoDomain = mockMovimentoDomain();

		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");

		tipoGastoRepository.saveAndFlush(tipoGastoModel);

		caixaRepository.saveAndFlush(caixaModel);
		
		// ação
		dataprovider.adicionarMovimento(movimentoDomain);

		// validação
		MovimentoModel movimento = repository.findAll().get(0);

		assertNotNull(movimento);
		assertEquals("PAO", movimento.getDescricaoMovimento());
		assertEquals(new BigDecimal("5.50"), movimento.getValor());
	}

	@Test
	public void dadoQueTenhoMovimentoPreenchidoComValorNegativo_entaoLancaException() {
		// cenário

		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");
		tipoGastoRepository.saveAndFlush(tipoGastoModel);

		caixaRepository.saveAndFlush(caixaModel);

		MovimentoDomain movimentoDomain = mockMovimentoInvalido();

		// validação
		RuntimeException error = assertThrows(RuntimeException.class, () -> {
			dataprovider.adicionarMovimento(movimentoDomain);
		});

		assertEquals("Ocorreu um erro ao realizar a gravação do movimento", error.getMessage());
	}

	@Test
	public void dadoQueTenhoMovimentosRegistrados_entaoRetornaListaGastos() {
		// cenário
		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");

		tipoGastoRepository.saveAndFlush(tipoGastoModel);

		caixaRepository.saveAndFlush(caixaModel);

		MovimentoModel movimentoModel = mockMovimentoModel();

		// ação
		repository.saveAndFlush(movimentoModel);

		MovimentoDomain movimento = dataprovider.getMovimentos().get(0);

		// validação
		assertNotNull(movimento);
		assertEquals(movimentoModel.getDescricaoMovimento(), movimento.getDescricaoMovimento());
		assertEquals(movimentoModel.getValor(), movimento.getValor());
	}

	@Test
	public void dadoQueTenhoMovimentos_entaoDeletaTodosGastos() {
		// cenário
		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");

		tipoGastoRepository.saveAndFlush(tipoGastoModel);

		caixaRepository.saveAndFlush(caixaModel);

		repository.save(mockMovimentoModel());

		// ação
		dataprovider.deletarMovimentos();

		// validação
		List<MovimentoModel> movimentos = repository.findAll();

		assertEquals(0, movimentos.size());
	}

	@Test
	public void dadoQueTenhoMovimentosRegistrados_filtroUmaDataValida_entaoRetornaRegistros() {
		// cenário
		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");

		LocalDate dataMovimento1 = LocalDate.of(2021, 05, 12);
		LocalDate dataMovimento2 = LocalDate.of(2021, 05, 30);

		tipoGastoRepository.saveAndFlush(tipoGastoModel);

		caixaRepository.saveAndFlush(caixaModel);

		MovimentoModel movimentoModel1 = mockMovimentoModel();
		movimentoModel1.setDataMovimento(dataMovimento1);
		repository.save(movimentoModel1);

		MovimentoModel movimentoModel2 = mockMovimentoModel();
		movimentoModel2.setDataMovimento(dataMovimento2);

		repository.save(movimentoModel2);

		// ação
		List<MovimentoDomain> movimentos = dataprovider.obterMovimentosPorData(dataMovimento1, dataMovimento2);

		// validação
		assertEquals(2, movimentos.size());
		assertEquals(movimentoModel1.getDescricaoMovimento(), movimentos.get(0).getDescricaoMovimento());
		assertEquals(movimentoModel2.getDescricaoMovimento(), movimentos.get(1).getDescricaoMovimento());

	}

	@Test
	public void dadoQueTenhoMovimentosRegistrados_ehForamGravadosAposDia27MesPassado_entaoRetornaResultados() {
		// cenário

		TipoGastoModel tipoGastoModel = new TipoGastoModel("OUTROS");
		tipoGastoRepository.saveAndFlush(tipoGastoModel);
		caixaRepository.saveAndFlush(caixaModel);

		MovimentoModel movimentoModel = mockMovimentoModel();
		movimentoModel.setDataMovimento(mockDataMovimento());

		MovimentoModel movimentoModel2 = mockMovimentoModel();
		movimentoModel2.setDataMovimento(mockDataMovimento());
		movimentoModel2.setDescricaoMovimento("UBER");

		MovimentoModel movimentoModel3 = mockMovimentoModel();
		movimentoModel3.setDataMovimento(LocalDate.now());
		movimentoModel3.setDescricaoMovimento("TIA CARMINHA");

		final String NOME_REGISTRO_DESPREZADO = "REGISTRO QUE DEVE SER DESPREZADO";
		// registro que não deve ser retornado no select
		MovimentoModel movimentoModel4 = mockMovimentoModel();
		movimentoModel4.setDataMovimento(LocalDate.now().minusMonths(2L));
		movimentoModel4.setDescricaoMovimento(NOME_REGISTRO_DESPREZADO);

		repository.saveAndFlush(movimentoModel);
		repository.saveAndFlush(movimentoModel2);
		repository.saveAndFlush(movimentoModel3);
		repository.saveAndFlush(movimentoModel4);

		// ação
		List<MovimentoDomain> movimentos = dataprovider.getMovimentos();

		Optional<MovimentoDomain> movimentoNaoObtido = movimentos.stream()
				.filter((m) -> m.getDescricaoMovimento().equals(NOME_REGISTRO_DESPREZADO)).findFirst();

		// validação
		assertEquals(3, movimentos.size());
		assertEquals(Optional.empty(), movimentoNaoObtido);

	}

	@Test
	public void dadoQueTenhoMovimentosPreenchidos_facoConsultaPorTipoEhPeriodo_entaoRetornaMovimentosRegistrados() {
		// cenário
		TipoGastoModel tipoGastoModel1 = new TipoGastoModel("ALIMENTACAO");
		tipoGastoModel1.setId(1L);
		TipoGastoModel tipoGastoModel2 = new TipoGastoModel("TRANSPORTE");
		tipoGastoModel2.setId(2L);

		tipoGastoRepository.saveAndFlush(tipoGastoModel1);
		tipoGastoRepository.saveAndFlush(tipoGastoModel2);
		caixaRepository.saveAndFlush(caixaModel);

		MovimentoModel movimentoModel = mockMovimentoModelTesteParametros("CESTA BÁSICA", tipoGastoModel1);

		MovimentoModel movimentoModel3 = mockMovimentoModelTesteParametros("LANCHE", tipoGastoModel1);

		MovimentoModel movimentoModel2 = mockMovimentoModelTesteParametros("UBER", tipoGastoModel2);

		repository.saveAndFlush(movimentoModel);
		repository.saveAndFlush(movimentoModel3);
		repository.saveAndFlush(movimentoModel2);

		// ação
		List<MovimentoDomain> movimentos = dataprovider.obterMovimentosDataEhTipo(mockParametros());

		// validação
		assertEquals(2, movimentos.size());

	}

	@Test
	public void dadoQueTenhoRegistroArmazenado_informoParametroSomenteTipoGasto_entaoRetornoMovimentoConsiderandoConsulta() {
		// cenário
		TipoGastoModel tipoGastoModel1 = new TipoGastoModel("ALIMENTACAO");
		tipoGastoModel1.setId(1L);
		TipoGastoModel tipoGastoModel2 = new TipoGastoModel("TRANSPORTE");
		tipoGastoModel2.setId(2L);

		tipoGastoRepository.saveAndFlush(tipoGastoModel1);
		tipoGastoRepository.saveAndFlush(tipoGastoModel2);
		caixaRepository.saveAndFlush(caixaModel);

		MovimentoModel movimentoModel = mockMovimentoModelTesteParametros("CESTA BÁSICA", tipoGastoModel1);

		MovimentoModel movimentoModel3 = mockMovimentoModelTesteParametros("LANCHE", tipoGastoModel1);

		MovimentoModel movimentoModel2 = mockMovimentoModelTesteParametros("UBER", tipoGastoModel2);

		repository.saveAndFlush(movimentoModel);
		repository.saveAndFlush(movimentoModel3);
		repository.saveAndFlush(movimentoModel2);

		// ação
		ParametrosConsultaMovimentoDomain parametros = mockParametros();

		// alterando pra considerar filtro somente pelo tipo gasto
		parametros.setTipoGastoId(2L);
		parametros.setDataFinal(null);
		parametros.setDataInicial(null);

		List<MovimentoDomain> movimentos = dataprovider.obterMovimentosDataEhTipo(parametros);

		// validação
		assertEquals(1, movimentos.size());
		assertEquals("UBER", movimentos.get(0).getDescricaoMovimento());
	}

	private MovimentoModel mockMovimentoModelTesteParametros(String descricao, TipoGastoModel tipoGasto) {
		MovimentoModel movimentoModel = new MovimentoModel();
		movimentoModel.setDataMovimento(mockDataMovimento());
		movimentoModel.setDescricaoMovimento(descricao);
		movimentoModel.setTipoGastoModel(tipoGasto);
		movimentoModel.setCaixaModel(caixaModel);

		return movimentoModel;
	}

	private ParametrosConsultaMovimentoDomain mockParametros() {
		ParametrosConsultaMovimentoDomain parametros = new ParametrosConsultaMovimentoDomain();
		parametros.setDataInicial(mockDataMovimento());
		parametros.setDataFinal(LocalDate.now());
		parametros.setTipoGastoId(1L);

		return parametros;
	}

	private LocalDate mockDataMovimento() {

		if (LocalDate.now().getDayOfMonth() < 27)
			return LocalDate.now().minusMonths(1L).withDayOfMonth(1).plusDays(26L);

		return LocalDate.now().withDayOfMonth(1).plusDays(26L);
	}

	private MovimentoModel mockMovimentoModel() {
		MovimentoModel movimentoModel = new MovimentoModel();
		movimentoModel.setDataMovimento(LocalDate.now());
		movimentoModel.setDescricaoMovimento("Churros Doce de Leite");
		movimentoModel.setValor(new BigDecimal("6.20"));

		TipoGastoModel tipoGastoModel = new TipoGastoModel("UBER");
		tipoGastoModel.setId(1L);

		movimentoModel.setCaixaModel(caixaModel);

		movimentoModel.setTipoGastoModel(tipoGastoModel);

		return movimentoModel;
	}

	private MovimentoDomain mockMovimentoDomain() {
		return new MovimentoDomain("PAO", new BigDecimal("5.50"), "2020-02-10", 1, uuidCaixa);
	}

	private MovimentoDomain mockMovimentoInvalido() {
		return new MovimentoDomain("PAO", new BigDecimal(-100), "2020-02-10", 1, uuidCaixa);
	}
}
