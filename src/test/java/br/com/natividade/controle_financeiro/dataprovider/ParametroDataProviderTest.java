package br.com.natividade.controle_financeiro.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;
import br.com.natividade.controle_financeiro.dataprovider.model.enums.TipoParametroEnum;
import br.com.natividade.controle_financeiro.dataprovider.repository.ParametroRepository;

@DataJpaTest
public class ParametroDataProviderTest {

	private ParametroDataProvider parametroDataProvider;

	@Autowired
	private ParametroRepository parametroRepository;

	private UUID uuidGerado = UUID.fromString("5ec18477-4cdd-4551-a02f-d5027c583167");

	@BeforeEach
	public void setUp() {
		parametroDataProvider = new ParametroDataProvider(parametroRepository);
	}

	@Test
	public void dadoQueTenhoParametroPreenchido_entaoRealizaGravacao() {
		// cenário
		ParametroModel parametroModel = mockParametroModel();

		// ação
		parametroDataProvider.adicionaParametro(parametroModel);

		// validação
		List<ParametroModel> parametros = parametroRepository.findAll();

		assertEquals(1, parametros.size());
		assertEquals(parametroModel.getValor(), parametros.get(0).getValor());
		assertEquals(parametroModel.getTipoParametro(), parametros.get(0).getTipoParametro());

	}

	@Test
	public void dadoQueTenhoParametroGravao_entaoRetornaPassandoTipo() {
		// cenário
		ParametroModel model = mockParametroModel();
		ParametroModel model2 = mockParametroModel();
		model2.setTipoParametro(TipoParametroEnum.REMETENTE_MOVIMENTO);

		parametroRepository.saveAndFlush(model2);
		parametroRepository.saveAndFlush(model);

		// ação
		ParametroModel parametroRetornado = parametroDataProvider
				.getParametroPorEnum(TipoParametroEnum.DESTINARIO_MOVIMENTO);

		// validação
		assertNotNull(parametroRetornado);
		assertEquals(model.getValor(), parametroRetornado.getValor());
		assertEquals(model.getTipoParametro(), parametroRetornado.getTipoParametro());
	}

	@Test
	public void dadoQueTentoProcuraParametroInexistente_entaoRetornaErroAoBuscar()
	{
		// cenário e ação
		RuntimeException error = assertThrows(RuntimeException.class, () -> {
			parametroDataProvider.getParametroPorEnum(TipoParametroEnum.REMETENTE_MOVIMENTO);
		});
		
		assertEquals("Parâmetro não encontrado!", error.getMessage());
	}

	private ParametroModel mockParametroModel() {
		ParametroModel parametroModel = new ParametroModel(uuidGerado);
		parametroModel.setTipoParametro(TipoParametroEnum.DESTINARIO_MOVIMENTO);
		parametroModel.setValor("gui@teste.com.br");

		return parametroModel;
	}
}
