package br.com.natividade.controle_financeiro.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.CaixaRepository;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;

@DataJpaTest
@DirtiesContext
public class CaixaDataProviderTest {
	private CaixaDataProvider dataProvider;

	@Autowired
	private CaixaRepository repository;

	@BeforeEach
	public void setUp() {
		dataProvider = new CaixaDataProvider(repository);
	}

	@Test
	public void dadoQueTenhoCaixaDadosValidos_entaoEfetuaGravacao() {
		// cenário
		CaixaDomain domain = mockDomain();

		// ação
		dataProvider.criarCaixa(domain);

		CaixaModel caixa = repository.findAll().get(0);

		// validação
		assertNotNull(caixa);
		assertEquals("NUBANK", caixa.getDescricao());
		assertEquals(new BigDecimal("100.0"), caixa.getValor());
	}

	@Test
	public void dadoQueTenhoCaixaDadosInvalidos_entaoCapturaInception() {
		// cenário
		CaixaDomain domain = mockDomain();

		final String DESCRICAO_INVALIDA = "";

		domain.setDescricao(DESCRICAO_INVALIDA);

		// ação e validação
		RuntimeException error = assertThrows(RuntimeException.class, () -> {
			dataProvider.criarCaixa(domain);
		});

		assertEquals("Falha ao criar caixa", error.getMessage());
	}

	@Test
	public void dadoQueTenhoRegistroComDescricao_lancaException_aoTentarGravarComMesmaDescricao() {
		// cenário

		dataProvider.criarCaixa(mockDomain());

		// ação e validação

		RuntimeException error = assertThrows(RuntimeException.class, () -> {
			dataProvider.criarCaixa(mockModelViolacaoUnique());
		});

		assertEquals("Falha ao criar caixa", error.getMessage());
	}

	@Test
	public void dadoQueTenhoUmaContaCadastrada_tenhoValorPreenchido_entaoDeposita() {
		// cenário
		dataProvider.criarCaixa(mockDomain());

		CaixaModel model = repository.findAll().get(0);

		final BigDecimal VALOR_DEPOSITO = new BigDecimal("50.50");
		CaixaDomain domain = new CaixaDomain("NUBANK", VALOR_DEPOSITO, model.getId());

		// ação
		dataProvider.movimentar(domain.getValor(), domain.getId(), TipoMovimentacaoEnum.DEPOSITAR);

		CaixaModel caixaDepoisDeposito = repository.findById(domain.getId()).get();

		// validação
		assertNotNull(caixaDepoisDeposito);
		assertEquals(new BigDecimal("150.50"), caixaDepoisDeposito.getValor());
	}

	@Test
	public void dadoQueTenhoaUmaContaCadastrada_tenhoValorPreenchido_entaoRetiraDinheiro() {
		// cenário
		dataProvider.criarCaixa(mockDomain());

		CaixaModel model = repository.findAll().get(0);

		final BigDecimal VALOR_RETIRAR = new BigDecimal("50.50");
		CaixaDomain domain = new CaixaDomain("NUBANK", VALOR_RETIRAR, model.getId());

		// ação
		dataProvider.movimentar(domain.getValor(), domain.getId(), TipoMovimentacaoEnum.RETIRAR);

		CaixaModel caixaDepoisDeposito = repository.findById(domain.getId()).get();

		// validação
		assertNotNull(caixaDepoisDeposito);
		assertEquals(new BigDecimal("49.50"), caixaDepoisDeposito.getValor());

	}
	
	@Test
	public void dadoQueTenhoCaixaCadastrado_entaoDeveRetornar() {
		// cenário
		dataProvider.criarCaixa(mockDomain());
		
		// ação
		List<CaixaDomain> caixas = dataProvider.getCaixas();
		
		// validação
		assertEquals(1, caixas.size());
		
	}

	private CaixaDomain mockModelViolacaoUnique() {
		return new CaixaDomain("NUBANK", new BigDecimal("0.25"));
	}

	public CaixaDomain mockDomain() {
		return new CaixaDomain("Nubank", new BigDecimal("100.0"));
	}
}
