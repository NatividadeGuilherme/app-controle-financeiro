package br.com.natividade.controle_financeiro.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoCaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.CaixaRepository;
import br.com.natividade.controle_financeiro.dataprovider.repository.MovimentoCaixaRepository;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;

@DataJpaTest
public class MovimentoCaixaDataProviderTest {
	
	@Autowired
	private MovimentoCaixaRepository repository;
	
	@Autowired
	private CaixaRepository caixaRepository;
	
	private MovimentoCaixaDataProvider provider;
	
	private CaixaDataProvider caixaDataProvider;
	
	private CaixaModel caixa1;
	private UUID uuid1 = UUID.fromString("9c2ad84b-7062-4594-ade0-040474158a2e");
	private UUID uuid2 = UUID.fromString("6e8bbd0f-66c4-4a98-8602-444c26ce4b6a");
	private CaixaModel caixa2;
	
	
	@BeforeEach
	public void setUp() {
		caixaDataProvider = new CaixaDataProvider(caixaRepository);
		provider = new MovimentoCaixaDataProvider(repository, caixaDataProvider);
		caixa1 = new CaixaModel("TICKET", new BigDecimal("10.00"), uuid1);
		caixa2 = new CaixaModel("TICKET 2", new BigDecimal("12.00"), uuid2);
	}
	
	@Test
	public void dadoQueTenhoMovimentoCaixaPreenchido_entaoRealizaTransferencia() {
		// cenário
		caixaRepository.saveAndFlush(caixa1);
		caixaRepository.saveAndFlush(caixa2);
		
		MovimentoCaixaDomain domain = mockCaixaDomain();
		
		// provider 
		provider.registrarTransferencia(domain);
		
		// validação gravação movimento caixa
		MovimentoCaixaModel model = repository.findAll().get(0);
		
		assertEquals(uuid1, model.getCaixa1().getId());
		assertEquals(uuid2, model.getCaixa2().getId());
		assertEquals(domain.getValorTransferido(), model.getValorTransferido());
		
		// validação deposito em conta 1 e retirada conta 2
		CaixaModel caixaModel1 = caixaRepository.findById(uuid1).get();
		CaixaModel caixaModel2 = caixaRepository.findById(uuid2).get();
		
		assertEquals(new BigDecimal("6.50"), caixaModel1.getValor());
		assertEquals(new BigDecimal("15.50"), caixaModel2.getValor());
	}
	
	private MovimentoCaixaDomain mockCaixaDomain() {
		CaixaDomain caixaDomain1 = new CaixaDomain("", null);
		caixaDomain1.setId(uuid1);
		
		CaixaDomain caixaDomain2 = new CaixaDomain("", null);
		caixaDomain2.setId(uuid2);
		
		return new MovimentoCaixaDomain(caixaDomain1, caixaDomain2, new BigDecimal("3.50"));
	}
}
