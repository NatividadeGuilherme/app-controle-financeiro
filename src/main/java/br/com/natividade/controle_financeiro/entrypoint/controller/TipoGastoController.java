package br.com.natividade.controle_financeiro.entrypoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.natividade.controle_financeiro.entrypoint.controller.dto.TipoGastoDTO;
import br.com.natividade.controle_financeiro.usecase.gateway.TipoGastoUseCase;

@Controller
@RequestMapping("tipogasto")
public class TipoGastoController {

	@Autowired
	private TipoGastoUseCase usecase;
	
	@PostMapping("/cadastrar")
	public String formGravar(TipoGastoDTO tipoGasto) {
		usecase.adicionaTipoGasto(tipoGasto.converterDomain());
		
		return "redirect:/movimento/registrar";
	}
}
