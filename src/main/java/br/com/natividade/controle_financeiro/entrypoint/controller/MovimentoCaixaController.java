package br.com.natividade.controle_financeiro.entrypoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.natividade.controle_financeiro.entrypoint.controller.dto.MovimentoCaixaDTO;
import br.com.natividade.controle_financeiro.usecase.CaixaUseCase;
import br.com.natividade.controle_financeiro.usecase.MovimentoCaixaUseCase;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;

@Controller
@RequestMapping("movimentocaixa")
public class MovimentoCaixaController {

	@Autowired
	private MovimentoCaixaUseCase movimentoCaixaUseCase;
	
	@Autowired
	private CaixaUseCase caixaUseCase;

	@GetMapping("/transferir")
	public ModelAndView realizarTransferencia() {
		ModelAndView modelAndView = new ModelAndView("movimentocaixa/formTransferir");
		modelAndView.addObject("movimentocaixa", new MovimentoCaixaDTO());
		
		List<CaixaDomain> caixas = caixaUseCase.getCaixas();
		
		modelAndView.addObject("caixas", caixas);
		
		return modelAndView;
	}
	
	@PostMapping("/transferir")
	public String realizarTransferencia(MovimentoCaixaDTO dto) {
		movimentoCaixaUseCase.registrarTransferencia(dto.converter());
		
		return "redirect:/movimentocaixa";
	}
}
