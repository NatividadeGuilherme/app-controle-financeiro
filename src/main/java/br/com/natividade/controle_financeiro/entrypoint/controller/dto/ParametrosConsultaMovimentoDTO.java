package br.com.natividade.controle_financeiro.entrypoint.controller.dto;

import java.time.LocalDate;

import br.com.natividade.controle_financeiro.usecase.parameters.ParametrosConsultaMovimentoDomain;

public class ParametrosConsultaMovimentoDTO {
	private String dataInicial;
	private String dataFinal;
	private String tipoGastoId;

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getTipoGastoId() {
		return tipoGastoId;
	}

	public void setTipoGastoId(String tipoGastoId) {
		this.tipoGastoId = tipoGastoId;
	}

	public ParametrosConsultaMovimentoDomain converter() {
		ParametrosConsultaMovimentoDomain parametros = new ParametrosConsultaMovimentoDomain();
		parametros.setDataInicial(setarDataPeriodo(dataInicial));
		parametros.setDataFinal(setarDataPeriodo(dataFinal));
		parametros.setTipoGastoId(setarTipoGasto(tipoGastoId));
		
		return parametros;
	}

	private LocalDate setarDataPeriodo(String data) {
		if (data.isEmpty())
			return null;
		return LocalDate.parse(data);
	}

	private long setarTipoGasto(String tipoGasto) {
		if (tipoGasto.isEmpty())
			return 0;

		return Long.parseLong(tipoGasto);
	}
}
