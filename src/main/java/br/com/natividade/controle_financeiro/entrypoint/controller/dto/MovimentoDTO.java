package br.com.natividade.controle_financeiro.entrypoint.controller.dto;

import java.math.BigDecimal;
import java.util.UUID;

import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;

public class MovimentoDTO {
	
	private String descricaoMovimento;
	private String valorMovimento;
	private String dataMovimento;
	private long tipoGastoId;
	private UUID caixaId;
	
	public void setTipoGastoId(long tipoGastoId) {
		this.tipoGastoId = tipoGastoId;
	}
	
	public String getDescricaoMovimento() {
		return descricaoMovimento;
	}
	
	public String getValorMovimento() {
		return valorMovimento;
	}
	
	public void setCaixaId(UUID caixaId) {
		this.caixaId = caixaId;
	}
	
	public void setDescricaoMovimento(String descricaoMovimento) {
		this.descricaoMovimento = descricaoMovimento;
	}
	
	public void setValorMovimento(String valorMovimento) {
		this.valorMovimento = valorMovimento;
	}
	
	public void setDataMovimento(String dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
	
	public long getTipoGastoId() {
		return tipoGastoId;
	}
	
	public UUID getCaixaId() {
		return caixaId;
	}
	
	public MovimentoDomain toDomain() {
		return new MovimentoDomain(descricaoMovimento, converterValor(), dataMovimento, tipoGastoId, caixaId);
	}
	
	private BigDecimal converterValor() {
		String valorSemCifrao = valorMovimento.replace("R$", "");
		String valorSemPontos = valorSemCifrao.replace(".", "");
		String valorSemVirgula = valorSemPontos.replace(",", ".");
		
		return new BigDecimal(valorSemVirgula);
	}
}
