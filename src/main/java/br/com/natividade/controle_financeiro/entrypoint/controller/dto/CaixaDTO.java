package br.com.natividade.controle_financeiro.entrypoint.controller.dto;

import java.math.BigDecimal;
import java.util.UUID;

import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;

public class CaixaDTO {
	private UUID id;
	private String valorCaixa;
	private String descricao;

	public UUID getId() {
		return id;
	}

	public BigDecimal getValorCaixa() {
		return converterValor();
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setValorCaixa(String valorCaixa) {
		this.valorCaixa = valorCaixa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	private BigDecimal converterValor() {
		String valorSemCifrao = valorCaixa.replace("R$", "");
		String valorSemPontos = valorSemCifrao.replace(".", "");
		String valorSemVirgula = valorSemPontos.replace(",", ".");

		return new BigDecimal(valorSemVirgula);
	}

	public CaixaDomain converter() {
		return new CaixaDomain(descricao, converterValor());
	}
}
