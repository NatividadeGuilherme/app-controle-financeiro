package br.com.natividade.controle_financeiro.entrypoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.natividade.controle_financeiro.entrypoint.controller.dto.MovimentoDTO;
import br.com.natividade.controle_financeiro.entrypoint.controller.dto.ParametrosConsultaMovimentoDTO;
import br.com.natividade.controle_financeiro.usecase.CaixaUseCase;
import br.com.natividade.controle_financeiro.usecase.MovimentoUseCase;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;
import br.com.natividade.controle_financeiro.usecase.gateway.TipoGastoUseCase;

@Controller
@RequestMapping("movimento")
public class MovimentoController {

	@Autowired
	private MovimentoUseCase movimentoUseCase;

	@Autowired
	private TipoGastoUseCase tipoGastoUseCase;

	@Autowired
	private CaixaUseCase caixaUseCase;

	@GetMapping("/registrar")
	public ModelAndView formRegistrar() {
		ModelAndView modelAndView = new ModelAndView("movimento/formMovimento");

		modelAndView.addObject("movimento", new MovimentoDTO());

		modelAndView.addObject("tiposGastos", tipoGastoUseCase.getTipos());
		modelAndView.addObject("caixas", caixaUseCase.getCaixas());

		return modelAndView;
	}

	@PostMapping("/registrar")
	public String formGravar(MovimentoDTO movimento) {
		movimentoUseCase.adicionarMovimento(movimento.toDomain());

		return "redirect:/movimento/";
	}

	@GetMapping("/")
	public ModelAndView listar() {
		ModelAndView modelView = new ModelAndView("movimento/movimentos");

		List<MovimentoDomain> movimentos = movimentoUseCase.getMovimentos();
		List<TipoGastoDomain> tipos = tipoGastoUseCase.getTipos();

		modelView.addObject("movimentos", movimentos);
		modelView.addObject("tiposgastos", tipos);
		modelView.addObject("tipoGastoId", 1L);

		modelView.addObject("valortotal", movimentoUseCase.getValorTotalGastos(movimentos));

		return modelView;
	}

	@GetMapping("/movimentosPorParametros")
	public @ResponseBody List<MovimentoDomain> obterMovimentosData(ParametrosConsultaMovimentoDTO parametro) {
		return movimentoUseCase.obterMovimentosPorParametros(parametro.converter());
	}

	@GetMapping("/delete")
	public void deletarMovimentos() {
		movimentoUseCase.deletarMovimentos();
	}
}
