package br.com.natividade.controle_financeiro.entrypoint.controller.dto;

import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

public class TipoGastoDTO {
	private String descricao;
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public TipoGastoDomain converterDomain() {
		return new TipoGastoDomain(descricao);
	}
}
