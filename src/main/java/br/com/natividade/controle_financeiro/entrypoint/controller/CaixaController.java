package br.com.natividade.controle_financeiro.entrypoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.natividade.controle_financeiro.entrypoint.controller.dto.CaixaDTO;
import br.com.natividade.controle_financeiro.usecase.CaixaUseCase;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;

@Controller
@RequestMapping("/caixa")
public class CaixaController {
	
	@Autowired
	private CaixaUseCase caixaUseCase;
	
	@GetMapping("/depositar")
	public ModelAndView depositar() {
		ModelAndView modelAndView = new ModelAndView("caixa/formDepositar");
		modelAndView.addObject("caixas", caixaUseCase.getCaixas());
		modelAndView.addObject("caixaDTO", new CaixaDTO());
		
		return modelAndView;
	}
	
	@PostMapping("/depositar")
	public String depositar(CaixaDTO dto) {
		caixaUseCase.movimentar(dto.getValorCaixa(), dto.getId(), TipoMovimentacaoEnum.DEPOSITAR);
		
		return "redirect:/caixas/";
	}
	
	@PostMapping("/criar")
	public String criarCaixa(CaixaDTO dto) {
		caixaUseCase.criarCaixa(dto.converter());
		
		return "redirect:/movimento/registrar";
	}
}
