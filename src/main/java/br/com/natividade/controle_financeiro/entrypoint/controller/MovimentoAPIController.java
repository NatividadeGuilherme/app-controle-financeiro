package br.com.natividade.controle_financeiro.entrypoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.natividade.controle_financeiro.entrypoint.controller.dto.MovimentoDTO;
import br.com.natividade.controle_financeiro.usecase.MovimentoUseCase;

@RestController
@RequestMapping("movimentoAPI")
public class MovimentoAPIController {
	
	@Autowired
	private MovimentoUseCase movimentoUseCase;
	
	
	@PostMapping("/adicionar")
	public ResponseEntity<MovimentoDTO> registrarMovimento(@RequestBody MovimentoDTO movimento){
		
		movimentoUseCase.adicionarMovimento(movimento.toDomain());
		
		return ResponseEntity.ok(movimento);
		
	}
}
