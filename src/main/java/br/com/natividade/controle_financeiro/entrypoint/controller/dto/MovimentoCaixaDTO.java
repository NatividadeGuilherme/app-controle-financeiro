package br.com.natividade.controle_financeiro.entrypoint.controller.dto;

import java.math.BigDecimal;
import java.util.UUID;

import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;

public class MovimentoCaixaDTO {
	private UUID caixa1Id;
	private UUID caixa2Id;
	private String valorTransferido;

	public UUID getCaixa1Id() {
		return caixa1Id;
	}

	public UUID getCaixa2Id() {
		return caixa2Id;
	}

	public String getValorTransferido() {
		return valorTransferido;
	}

	public void setCaixa1Id(UUID caixa1Id) {
		this.caixa1Id = caixa1Id;
	}

	public void setCaixa2Id(UUID caixa2Id) {
		this.caixa2Id = caixa2Id;
	}

	public void setValorTransferido(String valorTransferido) {
		this.valorTransferido = valorTransferido;
	}

	public MovimentoCaixaDomain converter() {
		CaixaDomain caixa1 = new CaixaDomain("", null);
		caixa1.setId(caixa1Id);
		
		CaixaDomain caixa2 = new CaixaDomain("", null);
		caixa2.setId(caixa2Id);
		
		return new MovimentoCaixaDomain(caixa1, caixa2, converterValor());
	}

	private BigDecimal converterValor() {
		String valorSemCifrao = valorTransferido.replace("R$", "");
		String valorSemPontos = valorSemCifrao.replace(".", "");
		String valorSemVirgula = valorSemPontos.replace(",", ".");
		
		return new BigDecimal(valorSemVirgula);
	}
}
