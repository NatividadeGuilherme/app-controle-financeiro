package br.com.natividade.controle_financeiro.usecase.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.UUID;

public class MovimentoDomain {

	private LocalDate dataMovimento;
	private String descricaoMovimento;
	private BigDecimal valor;
	private long tipoGastoId;
	private TipoGastoDomain tipoGastoDomain;
	private UUID caixaId;
	private CaixaDomain caixaDomain;

	public MovimentoDomain(String descricaoMovimento, BigDecimal valor, String dataMovimentoTexto, long tipoGastoId, UUID caixaId) {
		dataMovimento = converterData(dataMovimentoTexto);
		this.descricaoMovimento = descricaoMovimento;
		this.valor = valor;
		this.tipoGastoId = tipoGastoId;
		this.caixaId = caixaId;
	}
	
	public MovimentoDomain(String descricaoMovimento, BigDecimal valor, LocalDate dataMovimento, TipoGastoDomain tipoGastoDomain,
			CaixaDomain caixaDomain) {
		this.descricaoMovimento = descricaoMovimento;
		this.valor = valor;
		this.dataMovimento = dataMovimento;
		this.tipoGastoDomain = tipoGastoDomain;
		this.caixaDomain = caixaDomain;
	}
	

	private LocalDate converterData(String dataMovimentoTexto) {
		if(dataMovimentoTexto.isEmpty())
			return LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		
		return LocalDate.parse(dataMovimentoTexto);
	}
	public LocalDate getDataMovimento() {
		return dataMovimento;
	}

	public String getDescricaoMovimento() {
		return descricaoMovimento;
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public TipoGastoDomain getTipoGastoDomain() {
		return tipoGastoDomain;
	}
	
	public long getTipoGastoId() {
		return tipoGastoId;
	}
	
	public UUID getCaixaId() {
		return caixaId;
	}
	
	public CaixaDomain getCaixaDomain() {
		return caixaDomain;
	}
	
	public void setTipoGastoId(long tipoGastoId) {
		this.tipoGastoId = tipoGastoId;
	}
}
