package br.com.natividade.controle_financeiro.usecase.domain;

public class TipoGastoDomain {
	private long id;
	private String descricao;
	
	public TipoGastoDomain(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
