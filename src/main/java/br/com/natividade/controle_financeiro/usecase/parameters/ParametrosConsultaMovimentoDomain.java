package br.com.natividade.controle_financeiro.usecase.parameters;

import java.time.LocalDate;

public class ParametrosConsultaMovimentoDomain {

	private LocalDate dataInicial;
	private LocalDate dataFinal;
	private long tipoGastoId;
	
	public LocalDate getDataInicial() {
		return dataInicial;
	}

	public LocalDate getDataFinal() {
		return dataFinal;
	}

	public long getTipoGastoId() {
		return tipoGastoId;
	}

	public void setDataInicial(LocalDate dataInicial) {
		this.dataInicial = dataInicial;
	}

	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}

	public void setTipoGastoId(long tipoGastoId) {
		this.tipoGastoId = tipoGastoId;
	}
	
	
}
