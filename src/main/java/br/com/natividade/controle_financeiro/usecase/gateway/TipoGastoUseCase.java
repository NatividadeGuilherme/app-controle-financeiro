package br.com.natividade.controle_financeiro.usecase.gateway;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

@Component
public class TipoGastoUseCase {
	private TipoGastoGateway tipoGastoGateway;

	@Autowired
	public TipoGastoUseCase(TipoGastoGateway tipoGastoGateway) {
		this.tipoGastoGateway = tipoGastoGateway;
	}

	public List<TipoGastoDomain> getTipos() {
		return tipoGastoGateway.getTipos();
	}

	public void adicionaTipoGasto(TipoGastoDomain tipo) {
		tipoGastoGateway.adicionaTipoGasto(tipo);
	}
}
