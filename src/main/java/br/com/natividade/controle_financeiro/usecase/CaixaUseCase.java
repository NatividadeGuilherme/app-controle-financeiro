package br.com.natividade.controle_financeiro.usecase;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;
import br.com.natividade.controle_financeiro.usecase.gateway.CaixaGateway;

@Component
public class CaixaUseCase {
	private CaixaGateway caixaGateway;

	@Autowired
	public CaixaUseCase(CaixaGateway caixaGateway) {
		this.caixaGateway = caixaGateway;
	}
	
	public void movimentar(BigDecimal valor, UUID id, TipoMovimentacaoEnum tipoMovimentacaoEnum) {
		caixaGateway.movimentar(valor, id, tipoMovimentacaoEnum);
	}

	public List<CaixaDomain> getCaixas() {
		return caixaGateway.getCaixas();
	}
	
	public void criarCaixa(CaixaDomain domain) {
		caixaGateway.criarCaixa(domain);
	}
}
