package br.com.natividade.controle_financeiro.usecase.enuns;

public enum TipoMovimentacaoEnum {
	DEPOSITAR,
	RETIRAR
}
