package br.com.natividade.controle_financeiro.usecase.gateway;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;

public interface CaixaGateway {
	void criarCaixa(CaixaDomain caixaDomain);
	void movimentar(BigDecimal valor, UUID id, TipoMovimentacaoEnum tipoMovimentacaoEnum);
	List<CaixaDomain> getCaixas();
}
