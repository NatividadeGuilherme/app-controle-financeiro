package br.com.natividade.controle_financeiro.usecase.domain;

import java.math.BigDecimal;

public class MovimentoCaixaDomain {
	private CaixaDomain caixa1;
	private CaixaDomain caixa2;
	private BigDecimal valorTransferido;
	
	public MovimentoCaixaDomain(CaixaDomain caixa1, CaixaDomain caixa, BigDecimal valorTransferencia) {
		super();
		this.caixa1 = caixa1;
		this.caixa2 = caixa;
		this.valorTransferido = valorTransferencia;
	}
	public CaixaDomain getCaixa1() {
		return caixa1;
	}
	public CaixaDomain getCaixa2() {
		return caixa2;
	}
	public BigDecimal getValorTransferido() {
		return valorTransferido;
	}
	
}
