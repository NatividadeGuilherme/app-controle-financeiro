package br.com.natividade.controle_financeiro.usecase.domain;

import java.math.BigDecimal;
import java.util.UUID;

public class CaixaDomain {
	private UUID id;
	private String descricao;
	private BigDecimal valor;

	public CaixaDomain(String descricao, BigDecimal valor, UUID id) {
		super();
		this.descricao = descricao;
		this.valor = valor;
		this.id = id;
	}

	public CaixaDomain(String descricao, BigDecimal valor) {
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public UUID getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
