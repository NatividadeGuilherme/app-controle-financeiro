package br.com.natividade.controle_financeiro.usecase;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.gateway.MovimentoGateway;
import br.com.natividade.controle_financeiro.usecase.parameters.ParametrosConsultaMovimentoDomain;

@Component
public class MovimentoUseCase {

	@Autowired
	private MovimentoGateway movimentoGateway;
	
	public void adicionarMovimento(MovimentoDomain domain) {
		movimentoGateway.adicionarMovimento(domain);
	}

	public List<MovimentoDomain> getMovimentos() {
		List<MovimentoDomain> movimentos = movimentoGateway.getMovimentos();
		
		if(movimentos == null)
			return new ArrayList<>();
		
		return obtemMovimentosOrdenados(movimentos);

	}
	
	public BigDecimal getValorTotalGastos(List<MovimentoDomain> movimentos) {
		if(movimentos.isEmpty())
			return new BigDecimal(0);
		
		 Double valor = movimentos.stream()
				.mapToDouble(movimento -> movimento.getValor().
						doubleValue()).sum();
		
		return new BigDecimal(valor);
	}
	
	private List<MovimentoDomain> obtemMovimentosOrdenados(List<MovimentoDomain> movimentos) {
		return movimentos.stream().sorted(Comparator.comparing(MovimentoDomain::getDataMovimento).reversed())
				.collect(Collectors.toList());
	}

	public void deletarMovimentos() {
		movimentoGateway.deletarMovimentos();
	}
	
	public List<MovimentoDomain> obterMovimentosPorData(LocalDate dataInicial, LocalDate dataFinal){
		List<MovimentoDomain> movimentos = movimentoGateway.obterMovimentosPorData(dataInicial, dataFinal);
		
		if(movimentos == null)
			return new ArrayList<>();
		
		return movimentos;
	}

	public List<MovimentoDomain> obterMovimentosPorParametros(ParametrosConsultaMovimentoDomain parametros){
		List<MovimentoDomain> movimentos = movimentoGateway.obterMovimentosDataEhTipo(parametros);
		
		if(movimentos == null)
			return new ArrayList<>();
		
		return movimentos;
	}
}
