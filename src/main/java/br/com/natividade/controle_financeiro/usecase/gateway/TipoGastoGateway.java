package br.com.natividade.controle_financeiro.usecase.gateway;

import java.util.List;

import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

public interface TipoGastoGateway {
	void adicionaTipoGasto(TipoGastoDomain tipo);
	List<TipoGastoDomain> getTipos();
}
