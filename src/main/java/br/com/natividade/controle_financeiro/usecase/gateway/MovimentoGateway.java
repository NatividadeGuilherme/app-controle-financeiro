package br.com.natividade.controle_financeiro.usecase.gateway;

import java.time.LocalDate;
import java.util.List;

import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.parameters.ParametrosConsultaMovimentoDomain;

public interface MovimentoGateway {
	void adicionarMovimento(MovimentoDomain movimento);
	List<MovimentoDomain> getMovimentos();
	List<MovimentoDomain> obterMovimentosPorData(LocalDate dataInicial, LocalDate dataFinal);
	List<MovimentoDomain> obterMovimentosDataEhTipo(ParametrosConsultaMovimentoDomain parametros);
	void deletarMovimentos();
}
