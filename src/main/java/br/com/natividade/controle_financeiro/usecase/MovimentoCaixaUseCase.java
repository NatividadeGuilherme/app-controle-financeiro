package br.com.natividade.controle_financeiro.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;
import br.com.natividade.controle_financeiro.usecase.gateway.MovimentoCaixaGateway;

@Component
public class MovimentoCaixaUseCase {
	
	@Autowired
	private MovimentoCaixaGateway gateway;
	
	public void registrarTransferencia(MovimentoCaixaDomain domain) {
		gateway.registrarTransferencia(domain);
	}
	
}
