package br.com.natividade.controle_financeiro.usecase.gateway;

import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;

public interface MovimentoCaixaGateway {
	void registrarTransferencia(MovimentoCaixaDomain domain);
}
