package br.com.natividade.controle_financeiro.dataprovider;

import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;
import br.com.natividade.controle_financeiro.dataprovider.model.enums.TipoParametroEnum;
import br.com.natividade.controle_financeiro.dataprovider.repository.ParametroRepository;

@Component
public class ParametroDataProvider {
	
	private ParametroRepository parametroRepository;
	
	public ParametroDataProvider(ParametroRepository parametroRepository) {
		this.parametroRepository = parametroRepository;
	}
	
	public ParametroModel getParametroPorEnum(TipoParametroEnum tipoParametro) {
		ParametroModel parametro = parametroRepository.obterPorEnum(tipoParametro.ordinal());
		
		if(parametro == null)
			throw new RuntimeException("Parâmetro não encontrado!");
		
		return parametro;
	}
	
	public void adicionaParametro(ParametroModel parametroModel) {
		try {
			parametroRepository.saveAndFlush(parametroModel);
		} catch (ConstraintViolationException e) {
			throw new RuntimeException("Falha ao gravar parametro!");
		}
	}
}
