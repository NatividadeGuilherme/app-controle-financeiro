package br.com.natividade.controle_financeiro.dataprovider;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.natividade.controle_financeiro.dataprovider.mapper.TipoGastoMapper;
import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.TipoGastoRepository;
import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;
import br.com.natividade.controle_financeiro.usecase.gateway.TipoGastoGateway;

@Component
public class TipoGastoDataProvider implements TipoGastoGateway {

	private TipoGastoRepository repository;

	@Autowired
	public TipoGastoDataProvider(TipoGastoRepository tipoGastoRepository) {
		this.repository = tipoGastoRepository;
	}

	@Override
	public void adicionaTipoGasto(TipoGastoDomain tipo) {
		try {
			TipoGastoModel tipoGasto = TipoGastoMapper.toModel(tipo);
			repository.saveAndFlush(tipoGasto);
		} catch (ConstraintViolationException e) {
			throw new RuntimeException("Ocorreu um erro ao realizar a gravação do tipo de gasto");
		}
	}

	@Override
	public List<TipoGastoDomain> getTipos() {
		return TipoGastoMapper.toTiposDomain(repository.findAll());
	}
}
