package br.com.natividade.controle_financeiro.dataprovider.provider.impl;

import br.com.natividade.controle_financeiro.dataprovider.ParametroDataProvider;
import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;
import br.com.natividade.controle_financeiro.dataprovider.model.enums.TipoParametroEnum;
import br.com.natividade.controle_financeiro.dataprovider.provider.EmailProvider;

public class EmailMovimentoImpl implements EmailProvider{

	private ParametroDataProvider parametroDataProvider;
	
	public EmailMovimentoImpl(ParametroDataProvider parametroRepository) {
		this.parametroDataProvider = parametroRepository;
	}
	
	@Override
	public ParametroModel obterParametro() {
		return parametroDataProvider.getParametroPorEnum(TipoParametroEnum.DESTINARIO_MOVIMENTO);
	}

}
