package br.com.natividade.controle_financeiro.dataprovider.config;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropriedadesEmailConfiguration {

	@Value("${email_remetente}")
	private String email;
	
	@Value("${pass_remetente}")
	private String pass;
	
	public String getEmail() {
		return email;
	}

	private Properties getConfiguracaoEmail() {
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "smtp.office365.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

		return props;
	}

	public Session getSessao() {
		return Session.getDefaultInstance(getConfiguracaoEmail(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, pass);
			}
		});
	}

}
