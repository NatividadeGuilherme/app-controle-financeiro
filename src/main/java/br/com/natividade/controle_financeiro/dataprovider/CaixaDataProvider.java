package br.com.natividade.controle_financeiro.dataprovider;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.natividade.controle_financeiro.dataprovider.mapper.CaixaMapper;
import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.CaixaRepository;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;
import br.com.natividade.controle_financeiro.usecase.gateway.CaixaGateway;

@Component
public class CaixaDataProvider implements CaixaGateway{

	private CaixaRepository repository;
	
	@Autowired
	public CaixaDataProvider(CaixaRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public void criarCaixa(CaixaDomain caixaDomain) {
		try {
			CaixaModel model = CaixaMapper.toModel(caixaDomain);
			
			repository.saveAndFlush(model);
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			throw new RuntimeException("Falha ao criar caixa", e);
		}
	}
	
	@Override
	@Transactional
	public void movimentar(BigDecimal valor, UUID id, TipoMovimentacaoEnum tipoMovimentacaoEnum) {
		if(tipoMovimentacaoEnum.name().equals("DEPOSITAR"))
			repository.depositar(valor, id);
		else
			repository.retirar(valor, id);
	}

	@Override
	public List<CaixaDomain> getCaixas() {
		return CaixaMapper.toDomains(repository.findAll());
	}
}
