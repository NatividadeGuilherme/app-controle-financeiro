package br.com.natividade.controle_financeiro.dataprovider.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoModel;
import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

public class MovimentoMapper {

	private MovimentoMapper() {
	}

	public static MovimentoModel toModel(MovimentoDomain domain) {

		MovimentoModel model = new MovimentoModel();
		model.setDataMovimento(domain.getDataMovimento());
		model.setDescricaoMovimento(domain.getDescricaoMovimento().toUpperCase());
		model.setValor(domain.getValor());
		
		TipoGastoModel tipoGastoModel = new TipoGastoModel();
		tipoGastoModel.setId(domain.getTipoGastoId());
		
		CaixaModel caixaModel = new CaixaModel();
		caixaModel.setId(domain.getCaixaId());
		
		model.setTipoGastoModel(tipoGastoModel);
		model.setCaixaModel(caixaModel);

		return model;
	}

	public static List<MovimentoDomain> toDomains(List<MovimentoModel> movimentos){
		List<MovimentoDomain> movimentosDomain = new ArrayList<>();
		
		if(movimentos.isEmpty()) {
			return null;
		}
		
		movimentos.forEach((movimento) ->{
			movimentosDomain.add(toDomain(movimento));	
		});
		
		return movimentosDomain;
	}

	private static MovimentoDomain toDomain(MovimentoModel model) {
		TipoGastoDomain tipoGastoDomain = TipoGastoMapper.getTipoDomain(model.getTipoGastoModel());
		CaixaDomain caixaDomain = CaixaMapper.toDomain(model.getCaixaModel());
		
		return new MovimentoDomain(model.getDescricaoMovimento(), model.getValor(), model.getDataMovimento(), tipoGastoDomain, caixaDomain);
	}
}
