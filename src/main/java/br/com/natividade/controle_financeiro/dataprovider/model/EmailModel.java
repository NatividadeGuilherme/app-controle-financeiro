package br.com.natividade.controle_financeiro.dataprovider.model;

import java.util.ArrayList;
import java.util.List;

public class EmailModel {

	public EmailModel(String titulo, String mensagem) {
		this.titulo = titulo;
		this.mensagem = mensagem;
	}

	private String titulo;
	private String mensagem;

	public List<String> destinatarios = new ArrayList<>();

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setDestinatarios(List<String> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public void adicionaDestinatario(String emailDestinatario) {
		this.destinatarios.add(emailDestinatario);
	}
}
