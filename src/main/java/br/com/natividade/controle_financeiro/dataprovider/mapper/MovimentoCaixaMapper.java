package br.com.natividade.controle_financeiro.dataprovider.mapper;

import java.util.UUID;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoCaixaModel;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;

public class MovimentoCaixaMapper {
	private MovimentoCaixaMapper() {}
	
	public static MovimentoCaixaModel toModel(MovimentoCaixaDomain domain) {
		return new MovimentoCaixaModel(UUID.randomUUID(), 
				new CaixaModel(domain.getCaixa1().getDescricao(), domain.getCaixa1().getValor(), domain.getCaixa1().getId()), 
				new CaixaModel(domain.getCaixa2().getDescricao(), domain.getCaixa2().getValor(), domain.getCaixa2().getId()), 
				domain.getValorTransferido());
	}
}
