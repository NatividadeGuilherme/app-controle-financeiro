package br.com.natividade.controle_financeiro.dataprovider.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoModel;

@Repository
public interface MovimentoRepository extends JpaRepository<MovimentoModel, Long>{
	
	@Query(nativeQuery = true, value = "SELECT * FROM #{#entityName} m WHERE "
			+ " m.data_movimento BETWEEN ?1 AND ?2 ORDER BY m.data_movimento DESC")
	List<MovimentoModel> obterMovimentosPorData(LocalDate dataInicial, LocalDate dataFinal);
	
}
