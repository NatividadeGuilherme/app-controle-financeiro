package br.com.natividade.controle_financeiro.dataprovider.provider;

import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;

public interface EmailProvider {
	ParametroModel obterParametro();
}
