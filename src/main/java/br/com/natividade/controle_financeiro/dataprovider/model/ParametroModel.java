package br.com.natividade.controle_financeiro.dataprovider.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.natividade.controle_financeiro.dataprovider.model.enums.TipoParametroEnum;

@Entity(name = "parametro")
@Table(name = "parametro")
@Valid
public class ParametroModel {
	
	public ParametroModel() {}
	
	public ParametroModel(UUID id) {
	 this.id = id;
	}
	
	@Id
	private UUID id;
	
	@NotNull
	private String valor;
	
	@NotNull
	@Column(unique = true)
	@Enumerated(EnumType.ORDINAL)
	private TipoParametroEnum tipoParametro;

	public UUID getId() {
		return id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public TipoParametroEnum getTipoParametro() {
		return tipoParametro;
	}

	public void setTipoParametro(TipoParametroEnum tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

}
