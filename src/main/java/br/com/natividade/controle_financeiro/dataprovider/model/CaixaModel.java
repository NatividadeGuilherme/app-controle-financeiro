package br.com.natividade.controle_financeiro.dataprovider.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Entity(name = "caixa")
@Table
@Valid
public class CaixaModel {

	public CaixaModel(@NotEmpty String descricao, BigDecimal valor, UUID id) {
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
	}

	public CaixaModel() {}
	
	@Id
	private UUID id;

	@NotEmpty
	@Column(unique = true)
	private String descricao;

	private BigDecimal valor;
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
}
