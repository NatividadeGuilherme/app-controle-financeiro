package br.com.natividade.controle_financeiro.dataprovider;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.natividade.controle_financeiro.dataprovider.config.PropriedadesEmailConfiguration;
import br.com.natividade.controle_financeiro.dataprovider.helper.ThreadEmail;
import br.com.natividade.controle_financeiro.dataprovider.mapper.MovimentoMapper;
import br.com.natividade.controle_financeiro.dataprovider.model.EmailModel;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoModel;
import br.com.natividade.controle_financeiro.dataprovider.provider.impl.EmailMovimentoImpl;
import br.com.natividade.controle_financeiro.dataprovider.repository.MovimentoRepository;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;
import br.com.natividade.controle_financeiro.usecase.gateway.MovimentoGateway;
import br.com.natividade.controle_financeiro.usecase.parameters.ParametrosConsultaMovimentoDomain;

@Component
public class MovimentoDataProvider implements MovimentoGateway {

	private MovimentoRepository repository;
	private CaixaDataProvider caixaDataProvider;
	private EntityManager em;
	private PropriedadesEmailConfiguration configEmail;
	private EmailMovimentoImpl emailMovimentoImpl;
	
	public MovimentoDataProvider(MovimentoRepository repository, CaixaDataProvider caixaDataProvider,
			EntityManager em, PropriedadesEmailConfiguration configEmail, ParametroDataProvider parametroDataProvider) {
		this.repository = repository;
		this.caixaDataProvider = caixaDataProvider;
		this.em = em;
		this.configEmail = configEmail;
		this.emailMovimentoImpl = new EmailMovimentoImpl(parametroDataProvider);
	}

	@Override
	@Transactional
	public void adicionarMovimento(MovimentoDomain domain) {
		try {
			MovimentoModel movimentoModel = MovimentoMapper.toModel(domain);

			repository.saveAndFlush(movimentoModel);

			caixaDataProvider.movimentar(domain.getValor(), domain.getCaixaId(), TipoMovimentacaoEnum.RETIRAR);
						
			new Thread(new ThreadEmail(configEmail, getEmail(movimentoModel), emailMovimentoImpl)).start();
		} catch (ConstraintViolationException e) {
			throw new RuntimeException("Ocorreu um erro ao realizar a gravação do movimento");
		}
	}
	
	private EmailModel getEmail(MovimentoModel movimentoModel) {
		return new EmailModel("REGISTRO DE UM NOVO GASTO!", criarMensagemEmail(movimentoModel));
	}
	
	private String criarMensagemEmail(MovimentoModel movimentoModel) {
		return "Prezados(as), espero que estejam bem. \n "
			  +"Segue informações do gasto: \n "
			  +"Descrição: " + movimentoModel.getDescricaoMovimento() + "\n"
			  +"Valor: R$ " + movimentoModel.getValor() + "\n" 
			  +"Horário: " + movimentoModel.getDataMovimento();
	}

	@Override
	public void deletarMovimentos() {
		repository.deleteAll();
	}

	@Override
	public List<MovimentoDomain> getMovimentos() {
		LocalDate ultimoDia27 = mockDataMovimento();
		return MovimentoMapper.toDomains(repository.obterMovimentosPorData(ultimoDia27, LocalDate.now()));
	}

	private LocalDate mockDataMovimento() {

		if (LocalDate.now().getDayOfMonth() < 27)
			return LocalDate.now().minusMonths(1L).withDayOfMonth(1).plusDays(26L);

		return LocalDate.now().withDayOfMonth(1).plusDays(26L);
	}

	@Override
	public List<MovimentoDomain> obterMovimentosPorData(LocalDate dataInicial, LocalDate dataFinal) {
		List<MovimentoModel> movimentosModel = repository.obterMovimentosPorData(dataInicial, dataFinal);
		return MovimentoMapper.toDomains(movimentosModel);
	}

	@Override
	public List<MovimentoDomain> obterMovimentosDataEhTipo(ParametrosConsultaMovimentoDomain parametros) {
		String sql = " From movimento Where 1 = 1 ";
		sql = getStringConsultaPeriodoDatas(parametros, sql);
		sql = getStringConsultaTipoGasto(parametros, sql);
		sql += " Order by dataMovimento desc";

		Query query = em.createQuery(sql);

		if (parametros.getDataInicial() != null && parametros.getDataFinal() != null) {
			query.setParameter("dataInicial", parametros.getDataInicial());
			query.setParameter("dataFinal", parametros.getDataFinal());
		}

		if (parametros.getTipoGastoId() != 0) {
			query.setParameter("tipoGastoId", parametros.getTipoGastoId());
		}

		List<MovimentoModel> resultList = query.getResultList();

		return MovimentoMapper.toDomains(resultList);
	}

	private String getStringConsultaPeriodoDatas(ParametrosConsultaMovimentoDomain parametros, String query) {
		if (parametros.getDataFinal() != null && parametros.getDataInicial() != null)
			query += "And dataMovimento Between :dataInicial And :dataFinal ";

		return query;
	}

	private String getStringConsultaTipoGasto(ParametrosConsultaMovimentoDomain parametros, String query) {
		if (parametros.getTipoGastoId() != 0)
			query += "And tipogasto_id = :tipoGastoId ";

		return query;
	}

}
