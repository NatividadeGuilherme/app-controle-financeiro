package br.com.natividade.controle_financeiro.dataprovider.helper;

import java.util.Arrays;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.natividade.controle_financeiro.dataprovider.config.PropriedadesEmailConfiguration;
import br.com.natividade.controle_financeiro.dataprovider.model.EmailModel;
import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;
import br.com.natividade.controle_financeiro.dataprovider.provider.EmailProvider;

public class ThreadEmail implements Runnable {

	private PropriedadesEmailConfiguration configuration;

	private EmailModel email;

	private EmailProvider emailProvider;

	public ThreadEmail(PropriedadesEmailConfiguration configuration, 
			EmailModel email,
			EmailProvider emailProvider) {
		this.configuration = configuration;
		this.email = email;
		this.emailProvider = emailProvider;
	}

	@Override
	public void run() {

		try {
			Message message = new MimeMessage(configuration.getSessao());
			message.setFrom(new InternetAddress(configuration.getEmail()));

			adicionarDestinatarios(message);
			message.setSubject(email.getTitulo());
			message.setText(email.getMensagem());

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	private void adicionarDestinatarios(Message message) {
		try {
			for (String emailDestinatario : getDestinatarios()) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestinatario));
			}
		} catch (MessagingException e) {
			throw new RuntimeException("Falha ao adicionar destinatário para disparo de e-mail", e);
		}
	}
	
	private List<String> getDestinatarios(){
		ParametroModel parametroModel = emailProvider.obterParametro();
		
		return Arrays.asList(parametroModel.getValor().split(","));
	}
}