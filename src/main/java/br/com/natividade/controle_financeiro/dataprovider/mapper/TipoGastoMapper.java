package br.com.natividade.controle_financeiro.dataprovider.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;
import br.com.natividade.controle_financeiro.usecase.domain.TipoGastoDomain;

public class TipoGastoMapper {

	public static TipoGastoModel toModel(TipoGastoDomain tipoGastoDomain) {
		TipoGastoModel tipoGastoModel = new TipoGastoModel(tipoGastoDomain.getDescricao().toUpperCase());

		return tipoGastoModel;
	}

	public static List<TipoGastoDomain> toTiposDomain(List<TipoGastoModel> tiposModel) {
		List<TipoGastoDomain> tiposDomain = new ArrayList<>();

		tiposModel.forEach((tipo) -> {
			tiposDomain.add(getTipoDomain(tipo));
		});
		
		return tiposDomain;
	}

	public static TipoGastoDomain getTipoDomain(TipoGastoModel tipoModel) {

		TipoGastoDomain tipoGastoDomain = new TipoGastoDomain(tipoModel.getDescricao());
		tipoGastoDomain.setId(tipoModel.getId());

		return tipoGastoDomain;
	}

}
