package br.com.natividade.controle_financeiro.dataprovider.repository;

import java.math.BigDecimal;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;

@Repository
public interface CaixaRepository extends JpaRepository<CaixaModel, UUID>{
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true ,value = "update #{#entityName} SET valor = valor + ?1 where id = ?2 ")
	void depositar(BigDecimal valor, UUID id);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update #{#entityName} SET valor = valor - ?1 where id = ?2 ")
	void retirar(BigDecimal valor, UUID id);
}
