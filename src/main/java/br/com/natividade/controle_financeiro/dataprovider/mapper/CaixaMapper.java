package br.com.natividade.controle_financeiro.dataprovider.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import br.com.natividade.controle_financeiro.dataprovider.model.CaixaModel;
import br.com.natividade.controle_financeiro.usecase.domain.CaixaDomain;

public class CaixaMapper {
	private CaixaMapper() {}
	
	public static CaixaModel toModel(CaixaDomain domain) {
		return new CaixaModel(domain.getDescricao().toUpperCase(), domain.getValor(), UUID.randomUUID());
	}
	
	public static List<CaixaDomain> toDomains(List<CaixaModel> models){
		ArrayList<CaixaDomain> domains = new ArrayList<CaixaDomain>();
		
		models.forEach((caixa) ->{
			domains.add(toDomain(caixa));
		});
		
		return domains;
	}	
	public static CaixaDomain toDomain(CaixaModel model) {
		return new CaixaDomain(model.getDescricao(), model.getValor(), model.getId());
	}
}
