package br.com.natividade.controle_financeiro.dataprovider.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity(name = "movimento")
@Table(name = "movimento")
public class MovimentoModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String descricaoMovimento;
	
	private LocalDate dataMovimento;
	
	@Min((long) 0.1)
	private BigDecimal valor;
	
	@ManyToOne()
	@JoinColumn(name = "tipogasto_id")
	@NotNull
	private TipoGastoModel tipoGastoModel;
	
	@ManyToOne()
	@JoinColumn(name = "caixa_id")
	private CaixaModel caixaModel;
	
	public void setDataMovimento(LocalDate dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public void setDescricaoMovimento(String descricaoMovimento) {
		this.descricaoMovimento = descricaoMovimento;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public String getDescricaoMovimento() {
		return descricaoMovimento;
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public LocalDate getDataMovimento() {
		return dataMovimento;
	}
	
	public void setTipoGastoModel(TipoGastoModel tipoGastoModel) {
		this.tipoGastoModel = tipoGastoModel;
	}
	
	public TipoGastoModel getTipoGastoModel() {
		return tipoGastoModel;
	}
	
	public CaixaModel getCaixaModel() {
		return caixaModel;
	}

	public void setCaixaModel(CaixaModel caixaModel) {
		this.caixaModel = caixaModel;
	}
}
