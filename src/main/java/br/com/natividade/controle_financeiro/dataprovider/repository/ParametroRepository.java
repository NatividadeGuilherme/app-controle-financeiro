package br.com.natividade.controle_financeiro.dataprovider.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.natividade.controle_financeiro.dataprovider.model.ParametroModel;
import br.com.natividade.controle_financeiro.dataprovider.model.enums.TipoParametroEnum;

@Repository
public interface ParametroRepository extends JpaRepository<ParametroModel, UUID> {

	@Query(nativeQuery = true ,value = "select * from #{#entityName} Where tipo_parametro = ?1 ")
	ParametroModel obterPorEnum(int tipoParametro);
}
