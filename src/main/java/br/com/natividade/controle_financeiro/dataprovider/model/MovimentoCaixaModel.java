package br.com.natividade.controle_financeiro.dataprovider.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

@Entity(name = "movimentoscaixas")
@Validated
public class MovimentoCaixaModel {
	
	public MovimentoCaixaModel() {}
	
	public MovimentoCaixaModel(UUID id, @NotNull CaixaModel caixa1, @NotNull CaixaModel caixa2,
			BigDecimal valorTransferido) {
		this.id = id;
		this.caixa1 = caixa1;
		this.caixa2 = caixa2;
		this.valorTransferido = valorTransferido;
		this.dataHora = LocalDateTime.now();
	}

	@Id
	private UUID id;
	
	@ManyToOne
	@JoinColumn(name = "caixa1")
	@NotNull
	private CaixaModel caixa1;
	
	@ManyToOne
	@JoinColumn(name = "caixa2")
	@NotNull
	private CaixaModel caixa2;
	
	private LocalDateTime dataHora;
	
	private BigDecimal valorTransferido;

	public UUID getId() {
		return id;
	}

	public CaixaModel getCaixa1() {
		return caixa1;
	}

	public CaixaModel getCaixa2() {
		return caixa2;
	}

	public LocalDateTime getDataHora() {
		return dataHora;
	}
	
	public BigDecimal getValorTransferido() {
		return valorTransferido;
	}
	
	
}
