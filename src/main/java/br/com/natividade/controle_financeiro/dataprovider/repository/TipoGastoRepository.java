package br.com.natividade.controle_financeiro.dataprovider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.natividade.controle_financeiro.dataprovider.model.TipoGastoModel;

@Repository
public interface TipoGastoRepository extends JpaRepository<TipoGastoModel, Long>{

}
