package br.com.natividade.controle_financeiro.dataprovider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.natividade.controle_financeiro.dataprovider.mapper.MovimentoCaixaMapper;
import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoCaixaModel;
import br.com.natividade.controle_financeiro.dataprovider.repository.MovimentoCaixaRepository;
import br.com.natividade.controle_financeiro.usecase.domain.MovimentoCaixaDomain;
import br.com.natividade.controle_financeiro.usecase.enuns.TipoMovimentacaoEnum;
import br.com.natividade.controle_financeiro.usecase.gateway.MovimentoCaixaGateway;

@Component
public class MovimentoCaixaDataProvider implements MovimentoCaixaGateway {
	private MovimentoCaixaRepository repository;
	private CaixaDataProvider caixaDataProvider;
//vai
	@Autowired
	public MovimentoCaixaDataProvider(MovimentoCaixaRepository movimentoCaixaRepository,
			CaixaDataProvider caixaDataProvider) {
		repository = movimentoCaixaRepository;
		this.caixaDataProvider = caixaDataProvider;

	}

	@Override
	@Transactional
	public void registrarTransferencia(MovimentoCaixaDomain domain) {
		repassarValorCaixas(domain);

		MovimentoCaixaModel model = MovimentoCaixaMapper.toModel(domain);

		repository.saveAndFlush(model);
	}

	private void repassarValorCaixas(MovimentoCaixaDomain domain) {
		caixaDataProvider.movimentar(domain.getValorTransferido(), domain.getCaixa1().getId(),
				TipoMovimentacaoEnum.RETIRAR);

		caixaDataProvider.movimentar(domain.getValorTransferido(), domain.getCaixa2().getId(),
				TipoMovimentacaoEnum.DEPOSITAR);
	}

}
