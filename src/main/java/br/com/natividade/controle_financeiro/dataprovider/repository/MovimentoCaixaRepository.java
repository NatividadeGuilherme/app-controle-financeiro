package br.com.natividade.controle_financeiro.dataprovider.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.natividade.controle_financeiro.dataprovider.model.MovimentoCaixaModel;

@Repository
public interface MovimentoCaixaRepository extends JpaRepository<MovimentoCaixaModel, UUID>{

}
